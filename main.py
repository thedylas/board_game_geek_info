import requests
from get import simple_get
import bs4
import re
import csv
import numpy as np
from openpyxl import Workbook


def get_main_site():
    raw_html = simple_get('https://www.boardgamegeek.com/browse/boardgame.html')
    return raw_html


def get_board_game(url):
    game_raw_html = simple_get(url)
    return game_raw_html


def read_main_site(raw_html):
    list_of_urls = []
    soup = bs4.BeautifulSoup(raw_html, 'html5lib')
    all_games = soup.find_all(class_='collection_objectname')
    url = ''

    for i in range(1, 101):
        game = soup.find(id="results_objectname" + str(i))
        for a in game.find_all('a', href=True):
            tempurl = 'https://www.boardgamegeek.com' + (a['href'])
            list_of_urls.append(tempurl)

    return list_of_urls


def read_game_page(raw_html, list_of_game_info, game_url):
    soup = bs4.BeautifulSoup(raw_html, 'html5lib')

    board_game = {}

    # rank
    regex = re.compile(r'(\"(rank)\"\:\"([^\"]+)\")')
    board_game_rank = re.search(regex, str(raw_html))
    board_game_rank = int(board_game_rank.group(3))
    board_game["Rank"] = board_game_rank

    # name
    regex = re.compile(r'(\"([^\"]+)\"\:\"([^\"]+)\"\,\"(alternatename)\"\:)')
    board_game_name = re.search(regex, str(raw_html))

    # fix names
    regex_e = re.compile(r'\\\\u00e9')
    name_to_fix_e = re.search(regex_e, board_game_name.group())

    regex_parenthesis = re.compile(r'\\\'')
    name_to_fix_parenthesis = re.search(regex_parenthesis, board_game_name.group())

    if (name_to_fix_e):
        board_game_name = board_game_name.group(3)
        board_game_name = board_game_name.replace(name_to_fix_e.group(), "e")
        board_game["Name"] = board_game_name
    elif (name_to_fix_parenthesis):
        board_game_name = board_game_name.group(3)
        board_game_name = board_game_name.replace(name_to_fix_parenthesis.group(), "'")
        board_game["Name"] = board_game_name
    else:
        board_game["Name"] = board_game_name.group(3)

    # complexity
    regex = re.compile(r'((\w+)"\:)([\-\+]{0,1}\d[\d\.\,]*[\.\,][\d\.\,]*\d+)')
    board_game_complexity = re.search(regex, str(raw_html))
    board_game_complexity = round(float(board_game_complexity.group(3)))
    board_game["Complexity"] = board_game_complexity

    # players
    regex = re.compile(r'(\"(minplayers)\"\:\"([^\"]+)\"\,\"([^\"]+)\"\:\"([^\"]+)\")')
    board_game_players = re.search(regex, str(raw_html))
    board_game["Min players"] = int(board_game_players.group(3))
    board_game["Max players"] = int(board_game_players.group(5))

    # play time
    regex = re.compile(r'((minplaytime)\"([^\"]+)\"(\d+)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\d+))')
    board_game_time = re.search(regex, str(raw_html))
    board_game["Min play time"] = int(board_game_time.group(4))
    board_game["Max play time"] = int(board_game_time.group(8))

    # categories
    categories = set()
    regex = re.compile(
        r'(\{"(name)\"([^\"]+)\"(\w+|\w+ \w+|\w+ \\\\/ \w+)\"([^\"]+)\"(objecttype)\"([^\"]+)\"(property)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\d+)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\w+)\"([^\"]+)\"(\\\\/boardgamecategory))')
    board_game_categories = re.findall(regex, str(raw_html))

    # fix categories
    regex_slash = re.compile('\\\\\\\\/')

    for tuple in board_game_categories:
        category_to_fix = re.search(regex_slash, tuple[3])
        if category_to_fix:
            category_to_add = str(tuple[3])
            category_to_add = category_to_add.replace(category_to_fix.group(), "/")
            categories.add(category_to_add)
        else:
            categories.add(tuple[3])

    board_game["Categories"] = categories

    # url
    board_game["URL"] = game_url

    # append to list that was passed in to the function
    list_of_game_info.append(board_game)


def output(list_of_game_info):
    # create workbook
    wb = Workbook()
    ws = wb.active
    current_column = 'A'
    current_row = 2

    # basic header setup
    ws["A1"] = "Rank"
    ws["B1"] = "Name"
    ws["C1"] = "Complexity"
    ws["D1"] = "Min Players"
    ws["E1"] = "Max Players"
    ws["F1"] = "Min play time"
    ws["G1"] = "Max play time"
    ws["H1"] = "Categories"
    ws["I1"] = "URL"

    # input content
    for game in list_of_game_info:
        ws[current_column + str(current_row)] = game["Rank"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Name"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Complexity"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Min players"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Max players"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Min play time"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = game["Max play time"]
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        # category conversion to string
        string_of_categories = ''
        for category in game["Categories"]:
            string_of_categories += category + ", "
        string_of_categories = string_of_categories[0:len(string_of_categories) - 2]
        ws[current_column + str(current_row)] = string_of_categories
        current_column = 1 + ord(current_column)
        current_column = str(chr(current_column))

        ws[current_column + str(current_row)] = '=HYPERLINK("{}", "{}")'.format(game["URL"], game["URL"])
        ws[current_column + str(current_row)].style = "Hyperlink"

        # next game
        current_column = "A"
        current_row += 1

    # fix up workbook
    ws.auto_filter.ref = "A1:H101"
    ws.freeze_panes = ws["A2"]

    wb.save("board_game_geek.xlsx")


def __main__():
    list_of_game_info = []

    game_urls = read_main_site(get_main_site())
    for game_url in game_urls:
        read_game_page(get_board_game(game_url), list_of_game_info, game_url)

    output(list_of_game_info)


__main__()
