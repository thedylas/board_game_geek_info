# board_game_geek_info

This project gets the top 100 board game information and exports it to an excel sheet.

# Installation instructions
1. pip install requests
2. pip install bs4
3. pip install numpy
4. pip install html5lib
5. pip install openpyxl